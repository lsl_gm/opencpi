#!/bin/bash
set -e

CACHE_DIR=${CI_PROJECT_DIR}/.gitlab-ci/cache
mkdir -p $CACHE_DIR
if [[ ! -f $CACHE_DIR/.cachestamp ]] ; then 
    echo "No cache detected"
    rm -rf $CACHE_DIR/* 
    exit 0
fi
COMMIT_SHA=$(cat $CACHE_DIR/.cachestamp)
if [[ -z $CACHE_DIR ]] ; then
    echo "No commit SHA detected for cache. Not using cache."
    rm -rf $CACHE_DIR/*
fi
echo cache detected with commit SHA $COMMIT_SHA
FRAMEWORK_PATHS="bootstrap build os packaging platforms project-registry runtime scripts tests \
                 tools projects/**/prerequisites projects/**/rcc/platforms/${HOST} \
                 Framework.exports Makefile"
FRAMEWORK_CHANGES=$(git diff --name-only HEAD $COMMIT_SHA -- $FRAMEWORK_PATHS)
# If changes in framework detected, remove cache to start a clean build.
if [[ -n $FRAMEWORK_CHANGES ]] ; then
    echo "Not using cache. Framework changes detected."
    echo $FRAMEWORK_CHANGES
    rm -rf $CACHE_DIR/*
    exit 0
fi
echo "Using cache from commit SHA" $COMMIT_SHA
# Set cache to same timestamp
find $CACHE_DIR -exec touch -ht $(date +"%Y%m%d%H%M.%S") {} +
echo "Gathering changes since commit SHA" $COMMIT_SHA
# Gather changes
CHANGES_PATHS=$(git diff --name-only HEAD $COMMIT_SHA)
if [[ -z $CHANGES_PATHS ]] ; then
    echo "No changes detected."
    exit 0
fi
# Set changes to one second later than cache
for f in $CHANGES_PATHS; do 
    if [[ -e $f ]]; then # Ignore deletions
        touch -ht $(date +"%Y%m%d%H%M.%S" --date="+1 seconds") $f
        cp -r --parents $f $CACHE_DIR
    fi; 
done
