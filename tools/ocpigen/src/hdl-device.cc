/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include "wip.hh"
#include "hdl.hh"
#include "hdl-device.hh"
#include "hdl-slot.hh"
#include "hdl-platform.hh"
#include "assembly.hh"

//DeviceTypes DeviceType::s_types;

// The instancePVs argument is a pointer so that, when NULL, it indicates that
// no build/param configuration of the device is required
HdlDevice *HdlDevice::
create(ezxml_t xml, const char *xfile, const std::string &parentFile, Worker *parent,
       OM::Assembly::Properties *instancePVs, const char *&err) {
  if ((err = OE::checkTag(xml, "HdlDevice", "Expected 'HdlDevice' as tag in '%s'", xfile)) ||
      (err = OE::checkAttrs(xml, HDL_DEVICE_ATTRS, (void*)0)) ||
      (err = OE::checkElements(xml, HDL_DEVICE_ELEMS, (void*)0)))
    return NULL;
  HdlDevice *hd = new HdlDevice(xml, xfile, parentFile, parent, Worker::Device, instancePVs, err);
  if (err) {
    delete hd;
    hd = NULL;
  }
  return hd;
}

HdlDevice::
HdlDevice(ezxml_t xml, const char *file, const std::string &parentFile, Worker *parent,
	  Worker::WType type, OM::Assembly::Properties *instancePVs, const char *&err)
  : Worker(xml, file, parentFile, type, parent, instancePVs, err) {
  m_isDevice = true;
  if (err ||
      (err = OE::getBoolean(xml, "interconnect", &m_interconnect)) ||
      (err = OE::getBoolean(xml, "control", &m_canControl)) ||
      (err = parseHdl()))
    return;
  // Parse subdevice support for other device workers, also counting
  // how many of each supported type we support
  std::map<std::string, unsigned> countPerSupportedWorkerType;
  for (ezxml_t spx = ezxml_cchild(m_xml, "supports"); spx; spx = ezxml_cnext(spx)) {
    std::string worker;
    if ((err = OE::checkAttrs(spx, "worker", NULL)) ||
	(err = OE::checkElements(spx, "connect", NULL)) ||
	(err = OE::getRequiredString(spx, worker, "worker")))
      return;
    // Record how many each worker type we support and remember the ordinal
    auto pair = m_countPerSupportedWorkerType.insert(std::make_pair(worker, 0));
    if (!pair.second)
      pair.first->second++;
    // Since the "supports" relationship is not necessarily platform specific, the supported
    // worker is not associated with any build configuration or instance parameters in this XML.
    // A future feature could be that: any parameters mentioned in the <supports> XML
    // could be seen as constraints on the platform-specific build configuration
    // for the supported worker, both as an error check and as additional parameter settings.
    // So here we "get" this worker without regard to any build parameters or build configuration
    DeviceType *dt = get(worker.c_str(), file, parent, err);
    if (err) {
      err = OU::esprintf("for supported worker %s: %s", worker.c_str(), err);
      return;
    }
    m_supports.push_back(Support(*dt));
    // Record the ordinal of this supports element within those of the same device type
    m_supports.back().m_ordinal = pair.first->second;
    if ((err = m_supports.back().parse(spx, *this)))
      return;
    for (ezxml_t cx = ezxml_cchild(spx, "connect"); cx; cx = ezxml_cnext(cx)) {
      std::string l_port, to;
      size_t index;
      bool idxFound = false;
      if ((err = OE::checkAttrs(cx, "port", "to", "index", NULL)) ||
	  (err = OE::checkElements(cx, NULL)) ||
	  (err = OE::getRequiredString(cx, l_port, "port")) ||
	  (err = OE::getRequiredString(cx, to, "to")) ||
	  (err = OE::getNumber(cx, "index", &index, &idxFound)))
	return;
    }
  }
}

// This static method was intended to intern the device types, but now it doesn't,
// since each device on a board maybe parameterized/configured for that board.
// So now this method simply creates a new device-type-worker each time it is called.
// The name argument can be a file name.
HdlDevice *HdlDevice::
get(const char *a_name, const char *parentFile, Worker *parent, const char *&err) {
  // New device type, which must be a file.
  ezxml_t xml;
  std::string xfile;
  std::string name;
  const char *dot = strrchr(a_name, '.');
  if (dot) {
    if (strcmp(dot + 1, "hdl")) {
      err = OU::esprintf("Worker model for %s cannot be anything other than hdl here.", a_name);
      return NULL;
    }
    name.assign(a_name, OCPI_SIZE_T_DIFF(dot, a_name));
  } else
    name = a_name;
  // First look for the file assuming we are building a worker and the sought worker is in the
  // same library as we are.
  std::string
    file("../" + name + ".hdl/" + name),
    hdlFile(file + "-hdl"),
    hdlName(name + "-hdl");
  if (!(err = parseFile(file.c_str(), parentFile, NULL, &xml, xfile)) ||
      !(err = parseFile(name.c_str(), parentFile, NULL, &xml, xfile)) ||
      !(err = parseFile(hdlFile.c_str(), parentFile, NULL, &xml, xfile)) ||
      !(err = parseFile(hdlName.c_str(), parentFile, NULL, &xml, xfile)))
    return HdlDevice::create(xml, xfile.c_str(), parentFile, parent, NULL, err);
  return NULL;
}
const char *HdlDevice::
resolveExpressions(OB::IdentResolver &ir) {
  Worker::resolveExpressions(ir);
  const char *err;
  for (SignalsIter si = m_signals.begin(); si != m_signals.end(); si++) {
    Signal &s = **si;
    if (s.m_directionExpr.length() &&
	(err = s.parseDirection(s.m_directionExpr.c_str(), NULL, ir)))
      return OU::esprintf("parsing the signal direction for \"%s\": %s", s.cname(), err);
    if (s.m_widthExpr.length() &&
  	(err = s.parseWidth(s.m_widthExpr.c_str(), NULL, ir)))
      return OU::esprintf("parsing the signal width for \"%s\": %s", s.cname(), err);
  }
  // Now that we know the actual port counts, check port indices in supports XML
  for (auto sit = m_supports.begin(); sit != m_supports.end(); ++sit)
    for (auto cit = (*sit).m_connections.begin(); cit != (*sit).m_connections.end(); ++cit) {
      SupportConnection &c = *cit;
      if (c.m_sup_port->m_arrayCount && c.m_index >= c.m_sup_port->count())
	return OU::esprintf("Supported worker port \"%s\" has count %zu, index (%zu) too high",
			    c.m_sup_port->pname(), c.m_sup_port->count(), c.m_index);
    }
  return NULL;
}

void HdlDevice::
emitXmlSupports(std::string &out) const {
  for (auto sit = m_supports.begin(); sit != m_supports.end(); ++sit) {
    OU::formatAdd(out, "  <supports worker='%s'%s\n",
		  (*sit).m_type.cname(), (*sit).m_connections.size() ? ">" : "/>");
    for (auto cit = (*sit).m_connections.begin(); cit != (*sit).m_connections.end(); ++cit) {
      const SupportConnection &c = *cit;
      OU::formatAdd(out, "    <connect port='%s' to='%s'",
		    c.m_port->pname(), c.m_sup_port->pname());
      if (c.m_indexed)
	OU::formatAdd(out, " index='%zu'", c.m_index);
      out += "/>\n";
    }
    if ((*sit).m_connections.size())
      out += "  </supports>\n";
  }
}

const char *DeviceType::
cname() const {
  return m_implName;
}

const char *Signal::
decodeSignal(const std::string &name, std::string &base, size_t &index) {
  const char *sname = name.c_str();
  const char *paren = strchr(sname, '(');
  base = name;
  if (paren) {
    char *end;
    errno = 0;
    index = strtoul(paren + 1, &end, 0);
    if (errno != 0 || end == paren + 1 || *end != ')')
      return OU::esprintf("Bad numeric format in signal index: %s", sname);
    base.resize(OCPI_SIZE_T_DIFF(paren, sname));
  } else {
    index = SIZE_MAX;
  }
  return NULL;
}


// A device is not in its own file.
// It is an instance of a device type on a board
Device::
Device(Board &b, DeviceType &dt, const std::string &a_wname, ezxml_t xml, bool single,
       unsigned ordinal, SlotType *stype, const char *&err)
  : m_board(b), m_deviceType(dt), m_ordinal(ordinal), m_loadTime(false) {
  std::string wname(a_wname);
  const char *cp = strchr(wname.c_str(), '.');
  if (cp)
    wname.resize(OCPI_SIZE_T_DIFF(cp, wname.c_str()));
  if (!single)
    wname += "%u";
  OE::getNameWithDefault(xml, m_name, wname.c_str(), ordinal);
  if (!err)
    err = parse(xml, b, stype);
}

Device *Device::
create(Board &b, ezxml_t xml, const std::string &parentFile, Worker *parent, bool single,
       unsigned ordinal, SlotType *stype, const char *&err) {
  std::string wName;
  if ((err = OE::getRequiredString(xml, wName, "worker")))
    return NULL;
  // Do a first pass parse of any properties so that the device-type worker is created with the
  // provided properties.  No validation is done here except valid attributes.
  // These will be parsed again in parseProperties, so they are available when device instances are
  // created for platform configs and containers, possible with additional properties.
  // So this is a subset of what parseProperties does, but since it is done first, parseProperties
  // can do less error checking
  OM::Assembly::Properties instancePVs;
  std::map<std::string, ezxml_t, OCPI::Util::ConstStringCaseComp> xmlProperties;
  // First pass to eliminate platform-specific ones
  for (ezxml_t px = ezxml_cchild(xml, "Property"); px; px = ezxml_cnext(px)) {
    std::string l_name;
    if ((err = OE::checkAttrs(px, "name", "value", "valueFile", "default", "defaultFile",
			      "platform", NULL)) ||
	(err = OE::getRequiredString(px, l_name, "name", "property")))
      return NULL;
    const char *platform = ezxml_cattr(px, "platform");
    if (platform) {
      if (!stype) {
	err = OU::esprintf("the platform attribute for a device property is only valid for card devices");
	return NULL;
      }
      if (strcasecmp(platform, b.platform().cname()))
	continue;
    } else if (xmlProperties.find(l_name) != xmlProperties.end())
      continue; // properties with no platform never override earlier ones.
    xmlProperties[l_name] = px;
  }
  // Second pass has filtered and defaulted platform-specific values
  for (auto it = xmlProperties.begin(); it != xmlProperties.end(); ++it) {
    ezxml_t px = it->second;
    const char // valid attrs are filtered by caller provide allowable ones
      *value = ezxml_cattr(px, "value"),
      *valueFile = ezxml_cattr(px, "valueFile"),
      *defaultValue = ezxml_cattr(px, "default"),
      *defaultFile = ezxml_cattr(px, "defaultFile");
    switch (!!value + !!valueFile + !!defaultValue + !!defaultFile) {
    case 0:
      err = OU::esprintf("the property element for a device must have a one of these "
			   "attributes: \"value\", \"default\", \"valueFile\" or \"defaultFile\"");
      return NULL;
    default:
      err =
	  OU::esprintf("the property element for a platform or card device must have only one of "
		       "these attributes: \"value\", \"default\", \"valueFile\" or \"defaultFile\"");
      return NULL;
    case 1:;
    }
    std::string sValue;
    const char *file = valueFile ? valueFile : defaultFile;
    if (file) {
      if ((err = OU::file2String(sValue, file, ',')))
	return NULL;
    } else
      sValue = value ? value : defaultValue;
    instancePVs.resize(instancePVs.size() + 1);
    OM::Assembly::Property &p = instancePVs.back();
    p.m_name = it->first;
    p.m_value = sValue;
    p.m_hasValue = true;
  }
  Worker *w = Worker::create(wName.c_str(), parentFile, NULL, parent->m_outDir, parent, &instancePVs,
			     SIZE_MAX, err);
  if (!w) {
    err = OU::esprintf("for device worker %s in platform/card %s: %s", wName.c_str(),
		       parent->cname(), err);
    return NULL;
  }
  Device *dev = new Device(b, *static_cast<HdlDevice *>(w), wName, xml, single, ordinal, stype, err);
  if (err) {
    delete dev;
    dev = NULL;
  }
  return dev;
}

// A separate method because it is also called when a platform is instanced in a pf config
const char *Device::
parseSignalMappings(ezxml_t xml, Board &b, SlotType *stype) {
  const char *err;
  // Now we parse the mapping between device-type signals and board signals
  for (ezxml_t xs = ezxml_cchild(xml, "Signal"); xs; xs = ezxml_cnext(xs)) {
    std::string name, base;
    if (!OE::getOptionalString(xs, name, "name") && m_deviceType.m_type == Worker::Platform)
      continue;
    size_t index;
    if ((err = OE::getRequiredString(xs, name, "name")) ||
	(err = Signal::decodeSignal(name, base, index)))
      return err;
    std::string suffixed;
    // suffixed will be non-empty if we matched a suffixed signal, from diff or inout
    Signal *devSig = m_deviceType.m_sigmap.findSignal(base, &suffixed);
    if (!devSig)
      return OU::esprintf("Signal \"%s\" is not defined for device type \"%s\"",
			  base.c_str(), m_deviceType.cname());
    if (index != SIZE_MAX) {
      if (devSig->m_width == 0)
	return OU::esprintf("Device signal \"%s\" cannot be indexed", base.c_str());
      else if (index >= devSig->m_width)
	return OU::esprintf("Device signal \"%s\" has index higher than signal width (%zu)",
			    name.c_str(), devSig->m_width);
    }
    const char *plat = ezxml_cattr(xs, "platform");
    const char *card = ezxml_cattr(xs, "card"); // the slot
    if (!card)
      card = ezxml_cattr(xs, "slot");
    const char *board;
    Signal *boardSig = NULL; // the signal we are mapping a device signal TO
    size_t boardIndex;
    if (stype) {
      board = card;
      if (plat)
        return OU::esprintf("The platform attribute cannot be specified for signals on cards");
      else if (!card)
        return OU::esprintf("For signal \"%s\" for a device on a card, "
			   "the slot attribute must be specified", name.c_str());
      else if (*card && !(boardSig = b.m_boardSigMap.findSignal(card)))
	// The slot signal is not a signal for the slot type of this card
	return OU::esprintf("For signal \"%s\", the card signal \"%s\" is not defined "
			    "for slot type \"%s\"", name.c_str(), card, stype->cname());
      boardIndex = SIZE_MAX; // no indexing for card signals
    } else {
      board = plat;
      if (card)
	return OU::esprintf("For signal \"%s\" for a platform device, "
			    "the slot attribute cannot be specified", name.c_str());
      else if (!plat) {
	if (m_deviceType.m_type == Worker::Platform)
	  continue;
	return OU::esprintf("For signal \"%s\", the platform attribute must be specified",
			    name.c_str());
      } else if (*plat) {
	// FIXME: check for using the same signal in the same device
	// FIXME: specify mutex to allow same signal to be reused between mutex devices
	std::string l_name(plat), l_base;
	if ((err = Signal::decodeSignal(l_name, l_base, boardIndex)))
	  return err;
	if (!(boardSig = b.m_boardSigMap.findSignal(l_base.c_str()))) {
	  ocpiDebug("Adding platform signal: %s i.e. %s", board, l_base.c_str());
	  boardSig = new Signal();
	  boardSig->m_name = l_base;
	  boardSig->m_direction = devSig->m_direction;
	  boardSig->m_width =
	    boardIndex != SIZE_MAX ? boardIndex + 1 : // initial size is based on first index seen
	    index == SIZE_MAX ? devSig->m_width       // neither is indexed, copy dev signal width
	    : 0;                                      // dev signal index, size is 0
	  b.m_boardSigMap[l_base.c_str()] = boardSig;
	  b.m_boardSignals.push_back(boardSig);
	} else {
	  if (boardIndex != SIZE_MAX && boardIndex + 1 > boardSig->m_width)
	    boardSig->m_width = boardIndex + 1; // new index is widerm use it
	  ocpiDebug("Mapping to existing platform signal: %s i.e. %s", board, boardSig->cname());
	}
      }
    }
    // Check compatibility between device and slot/platform signal
    if (boardSig) {
      switch (boardSig->m_direction) {
      case Signal::IN: // input to board
	if (devSig->m_direction != Signal::IN && devSig->m_direction != Signal::UNUSED)
	  return OU::esprintf("Board signal \"%s\" is input to card/platform, "
			      "but \"%s\" is not input to device (its direction is: %s)",
			      boardSig->cname(), devSig->cname(),
			      Signal::directions[devSig->m_direction]);
	break;
      case Signal::OUT: // output from board
	if (devSig->m_direction != Signal::OUT && devSig->m_direction != Signal::UNUSED)
	  return OU::esprintf("Board signal \"%s\" is output from card/platform, "
			      "but \"%s\" is not output from device (its direction is: %s)",
			      boardSig->cname(), devSig->cname(),
			      Signal::directions[devSig->m_direction]);
	break;
      case Signal::INOUT: // FIXME: is this really allowed for slots?
	if (devSig->m_direction != Signal::INOUT && devSig->m_direction != Signal::UNUSED)
	  return OU::esprintf("Board signal \"%s\" is inout to/from card/platform, "
			      "but \"%s\" is not inout at device (its direction is: %s)",
			      boardSig->cname(), devSig->cname(),
			      Signal::directions[devSig->m_direction]);

	break;
      case Signal::BIDIRECTIONAL:
	// board signal is agnostic, with direction coming from device signal
#if 0
	if (devSig->m_direction == Signal::INOUT)
	  return OU::esprintf("Board signal \"%s\" is bidirectional to or from board, "
			      "but \"%s\" is inout at device", boardSig->cname(),
			      devSig->cname());
#endif
	break;
      case Signal::OUTIN:
	return OU::esprintf("Emulators can not be used with slots");
      default:	;
      }
      // Here board and boardSig are set
      Signal *other = b.m_bd2dev.findSignal(board);
      if (other) {
	// Multiple device signals to the same board signal.  The other signal was ok by itself.
	switch (other->m_direction) {
	case Signal::IN:
	  if (devSig->m_direction != Signal::IN)
	    return OU::esprintf("Multiple incompatible device signals assigned to \"%s\" signal",
				board);
	  break;
	case Signal::OUT:
	  return OU::esprintf("Multiple device output signals driving \"%s\" signal ", board);
	default:	  ;
	}
      } else {
	// First time we have seen this board signal
	// Map to find device signal and index from slot/board signal
	b.m_bd2dev.insert(board, devSig, index);
      }
    }
    // boardSig might be NULL here.
    std::string
      devSigIndexed = suffixed.empty() ? devSig->cname() : suffixed.c_str(),
      boardSigIndexed = boardSig ? boardSig->cname() : "";
    if (index != SIZE_MAX)
      OU::formatAdd(devSigIndexed, "(%zu)", index);
    if (boardIndex != SIZE_MAX)
      OU::formatAdd(boardSigIndexed, "(%zu)", boardIndex);
    m_strings.push_front(devSigIndexed);
    ocpiDebug("Mapping device signal %s to board signal %s",
	      devSigIndexed.c_str(), boardSigIndexed.c_str());
    m_dev2bd.push_back(devSig, index, boardSig ? boardSig->cname() : "", boardIndex);
  }
  // Now check for the unsupported condition where a signal is partially mapped.
  for (auto si = m_deviceType.m_signals.begin(); si != m_deviceType.m_signals.end(); si++) {
    Signal &s = **si;
    if (!s.m_width)
      continue;
    bool anyMapped = false, anyNotMapped = false;
    size_t firstMapped = 0, firstNotMapped = 0; // initializations for warnings
    for (size_t n = 0; n < s.m_width; ++n) {
      bool isWhole;
      size_t bdIndex;
      if (m_dev2bd.findSignal(s, n, isWhole, bdIndex)) {
	if (!anyMapped) {
	  anyMapped = true;
	  firstMapped = n;
	}
      } else if (!anyNotMapped) {
	anyNotMapped = true;
	firstNotMapped = n;
      }
    }
    if (anyMapped && anyNotMapped)
      return OU::esprintf("for vector signal \"%s\" width %zu, of %s device \"%s\" "
			  "worker \"%s\":  index %zu is mapped to a %s signal, "
			  "but index %zu is not:  all or none of the indices must be mapped",
			  s.cname(), s.m_width, stype ? "card" : "platform", cname(),
			  m_deviceType.cname(), firstMapped, stype ? "slot" : "platform", firstNotMapped);
  }
  return NULL;
}

// sType != NULL means the device is on a card
// Property values are parsed and validated and canonicalized.
// This is really a second pass over these properties so minimal error checking is done.
// Previous first pass was in Device::create
const char *Device::
parseProperties(ezxml_t x, const SlotType *sType) {
  const char *err;
  for (ezxml_t px = ezxml_cchild(x, "Property"); px; px = ezxml_cnext(px)) {
    const char *l_name = ezxml_cattr(px, "name");
    size_t nDummy;
    OM::Property *prop;
    if ((err = m_deviceType.findParamProperty(l_name, prop, nDummy)))
      return err;
    const char // valid attrs are filtered by caller provide allowable ones
      *value = ezxml_cattr(px, "value"),
      *valueFile = ezxml_cattr(px, "valueFile"),
      *defaultValue = ezxml_cattr(px, "default"),
      *defaultFile = ezxml_cattr(px, "defaultFile"),
      *platform = ezxml_cattr(px, "platform");
    if (platform && !sType)
      return OU::esprintf("the platform attribute for a device property is only valid for card devices");
    InstanceProperty param;
    param.m_property = prop;
    param.m_value.setType(*prop);
    param.m_platform = platform ? platform : "";
    param.m_isFixed = value || valueFile;
    std::string sValue;
    const char *file = valueFile ? valueFile : defaultFile;
    if (file) {
      if ((err = OU::file2String(sValue, file, ',')))
	return err;
    } else
      sValue = value ? value : defaultValue;
    if ((err = param.m_value.parse(sValue.c_str())))
      return err;
    param.m_value.unparse(param.m_uValue); // canonical/unparsed value
    m_parameters.push_back(param);
  }
  return NULL;
}

const char *Device::
parse(ezxml_t xml, Board &b, SlotType *stype) {
  const char *err;
  // This might happen in floating devices in containers.
  if (b.findDevice(m_name))
    return OU::esprintf("Duplicate device name \"%s\" for platform/card", m_name.c_str());
  // In pass 1 we don't know all devices on the platform so just record the strings
  for (ezxml_t xs = ezxml_cchild(xml, "supported"); xs; xs = ezxml_cnext(xs)) {
    std::string device;
    if ((err = OE::checkAttrs(xs, "device", NULL)) ||
	(err = OE::getRequiredString(xs, device, "device")))
      return err;
    m_supportedDevices.push_back(device);
  }
  size_t supportsCount = m_deviceType.m_supports.size();
  if (m_supportedDevices.size() && m_supportedDevices.size() != supportsCount)
    return OU::esprintf("For device \"%s\", there are not enough <supported> elements, should be %zu",
			m_name.c_str(), supportsCount);
  // So the loadtime attribute of the instance comes from the worker or the device
  if (m_deviceType.isLoadTime()) { // instance defaults from worker
    if (ezxml_cattr(xml, "loadtime"))
      return OU::esprintf("for device \"%s\", worker \"%s\" is already specified as load time",
			  cname(), m_deviceType.cname());
    m_loadTime = true;  // propagate from the worker to the device
  } else if ((err = OE::getBoolean(xml, "loadtime", &m_loadTime)))
    return err;
  // Save the parsed and validated properties in the Device object for application to
  // platform configurations and containers later.  stype indicates a card if not NULL
  if (m_deviceType.m_type != Worker::Platform && (err = parseProperties(xml, stype)))
    return err;
  return parseSignalMappings(xml, b, stype);
}

const char *Board::
addFloatingDevice(ezxml_t xs, const char *parentFile, Worker *parent, std::string &name) {
  const char *err = NULL;
  Device *dev = Device::create(*this, xs, parentFile, parent, true, 0, NULL, err);
  if (dev) {
    m_devices.push_back(dev);
    name = dev->cname();
  }
  return err;
}

// Add all the devices for a board - static
const char *Board::
parseDevices(ezxml_t xml, SlotType *stype, const std::string &parentFile, Worker *parent) {
  // These device declarations are saying which devices are part of the board (platform or card)
  for (ezxml_t xs = ezxml_cchild(xml, "Device"); xs; xs = ezxml_cnext(xs)) {
    const char *err;
    if ((err = OE::checkElements(xs, DEVICE_ELEMS, NULL)) ||
	(err = OE::checkAttrs(xs, DEVICE_ATTRS, NULL)))
      return err;
    const char *worker = ezxml_cattr(xs, "worker");
    bool single = true;
    bool seenMe = false;
    unsigned n = 0; // ordinal of devices of same type in this platform/card
    for (ezxml_t x = ezxml_cchild(xml, "Device"); x; x = ezxml_cnext(x)) {
      const char *w = ezxml_cattr(x, "worker");
      if (x == xs)
	seenMe = true;
      else if (worker && w && !strcasecmp(worker, w)) {
	single = false;
	if (!seenMe)
	  n++;
      }
    }
    Device *dev = Device::create(*this, xs, parentFile, parent, single, n, stype, err);
    if (dev)
      m_devices.push_back(dev);
    else
      return err;
  }
  // Second pass for error checking that all "supports map" entry point to real devices"
  for (auto di = m_devices.begin(); di != m_devices.end(); ++di) {
    Device &d = **di;
    d.m_supportsMap.resize(d.m_supportedDevices.size());
    unsigned n = 0;
    assert(d.m_supportedDevices.empty() ||
	   d.m_deviceType.m_supports.size() == d.m_supportedDevices.size());
    auto dtsi = d.m_deviceType.m_supports.begin();
    for (auto si = d.m_supportedDevices.begin(); si != d.m_supportedDevices.end(); ++si, ++dtsi, ++n) {
      const Device *supported = findDevice(*si);
      if (!supported)
	return OU::esprintf("In <supported> mapping element for device %s, actual device \"%s\" not found",
			    d.cname(), (*si).c_str());
      // So the mentioned device is a device on the board.  Is it the right type?
      if (strcasecmp(supported->m_deviceType.cname(), dtsi->m_type.cname()))
	return OU::esprintf("In <supported> mapping element #%u in device \"%s\", specified "
			    "device \"%s\" is the wrong device type, should be \"%s\"",
			    n, d.cname(), si->c_str(), dtsi->m_type.cname());
      d.m_supportsMap[n] = supported;
    }
  }
  return NULL;
}
// Static
const Device *Device::
find(const char *name, const Devices &devices) {
  for (DevicesIter di = devices.begin(); di != devices.end(); di++) {
    Device &dev = **di;
    if (!strcasecmp(name, dev.m_name.c_str()))
      return &dev;
  }
  return NULL;
}
#if 0
const Device &Device::
findSupport(const DeviceType &dt, unsigned ordinal, const Devices &devices) {
  for (DevicesIter di = devices.begin(); di != devices.end(); di++) {
    Device &dev = **di;
    // FIXME: intern the workers
    if (!strcasecmp(dev.m_deviceType.m_implName, dt.m_implName) && dev.m_ordinal == ordinal)
      return dev;
  }
  assert("Support (sub)device not found"==0);
  return *(Device*)NULL;
}
#endif
const Device *Board::
findDevice(const char *name) const {
  for (DevicesIter di = m_devices.begin(); di != m_devices.end(); di++) {
    Device &dev = **di;
    if (!strcasecmp(name, dev.cname()))
      return &dev;
  }
  return NULL;
}

SupportConnection::
SupportConnection()
  : m_port(NULL), m_sup_port(NULL), m_index(0), m_indexed(false) {
}

const char *SupportConnection::
parse(ezxml_t cx, Worker &w, Support &r) {
  const char *err;
  std::string port, to;
  if ((err = OE::checkAttrs(cx, "port", "signal", "to", "index", (void *)0)) ||
      (err = OE::checkElements(cx, NULL)) ||
      (err = OE::getRequiredString(cx, port, "port")) ||
      (err = OE::getRequiredString(cx, to, "to")) ||
      (err = OE::getNumber(cx, "index", &m_index, &m_indexed, 0, false)) ||
      (err = r.m_type.getPort(port.c_str(), m_port)) ||
      (err = w.getPort(to.c_str(), m_sup_port)))
    return err;
  if (m_sup_port->m_type != m_port->m_type)
    return OU::esprintf("Supported worker port \"%s\" is not the same type", to.c_str());
  if (m_sup_port->m_master == m_port->m_master)
    return OU::esprintf("Supported worker port \"%s\" has same role (master) as port \"%s\"",
			to.c_str(), port.c_str());
  if (m_sup_port->m_arrayCount) {
    if (!m_indexed)
      return OU::esprintf("Supported worker port \"%s\" has count, index must be specified",
			  to.c_str());
#if 0 // during early parsing counts are not resolved, so check is moved to resolveExpressions()
    if (m_index >= m_sup_port->count())
      return OU::esprintf("Supported worker port \"%s\" has count %zu, index (%zu) too high",
			  to.c_str(), m_sup_port->count(), m_index);
#endif
  }
  // FIXME: check signal compatibility...
  return NULL;
}

Support::
Support(const DeviceType &dt)
  : m_type(dt), m_ordinal(0) {
}

const char *Support::
parse(ezxml_t spx, Worker &w) {
  const char *err;
  std::string worker;
  if ((err = OE::checkAttrs(spx, "worker", (void *)0)) ||
      (err = OE::checkElements(spx, "connect", (void*)0)) ||
      (err = OE::getRequiredString(spx, worker, "worker")))
    return err;
  for (ezxml_t cx = ezxml_cchild(spx, "connect"); cx; cx = ezxml_cnext(cx)) {
    m_connections.push_back(SupportConnection());
    if ((err = m_connections.back().parse(cx, w, *this)))
      return err;
  }
  return NULL;
}

// This does instance parsing for instances of HdlDevice workers.
// There is no HdlInstance class, so it is done here for now.
const char *Worker::
parseInstance(Worker &parent, Instance &i, ezxml_t x) {
  const char *err;
  OE::getOptionalString(x, i.m_device, "device");
  if ((err = OE::getBoolean(x, "loadtime", &i.m_loadTime, true)))
    return err;
  // Signal mapping of device worker instances is only done in the container, even for devices
  // instanced in the platform configuration.
  for (ezxml_t sx = ezxml_cchild(x, "signal"); sx; sx = ezxml_cnext(sx)) {
    std::string l_name, base, external, extBase;
    size_t index, extIndex;
    if ((err = OE::getRequiredString(sx, l_name, "name")) ||
	(err = OE::getRequiredString(sx, external, "external")) ||
	(err = Signal::decodeSignal(external, extBase, extIndex)) ||
	(err = Signal::decodeSignal(l_name, base, index)))
      return err;
    Signal *s = m_sigmap.findSignal(base);
    if (!s)
      return OU::esprintf("Worker \"%s\" of instance \"%s\" has no signal \"%s\"",
			  m_implName, i.cname(), l_name.c_str());
    assert(index == SIZE_MAX || s->m_width);
    if (external.length()) {
      bool isWhole;
      size_t dummy;
      if (i.m_inst2ext.findSignal(*s, index, isWhole, dummy))
	return OU::esprintf("Duplicate signal \"%s\" for worker \"%s\" instance \"%s\"",
			    l_name.c_str(), m_implName, i.cname());
      if (i.m_inst2ext.findSignal(extBase, extIndex, dummy) && s->m_direction == Signal::OUT)
	return OU::esprintf("Multiple outputs drive external \"%s\" for worker \"%s\" "
			    "instance \"%s\"", external.c_str(), m_implName, i.cname());
      Signal *ps = parent.m_sigmap.findSignal(extBase);
      if (!ps)
	return OU::esprintf("External signal \"%s\" specified for signal \"%s\" of "
			    "instance \"%s\" of worker \"%s\" is not an external signal of the "
			    "assembly", external.c_str(), l_name.c_str(), i.cname(), m_implName);
      // If the board signal is bidirectional (can be anything), it should inherit
      // the direction of the device's signal
      if (ps->m_direction == Signal::BIDIRECTIONAL)
	ps->m_direction = s->m_direction;
      ps->m_pin = s->m_pin;
    }
    ocpiDebug("Instance '%s' signal '%s' index '%zu' mapped to '%s'",
	      i.cname(), s->cname(), index, external.c_str());
    i.m_inst2ext.push_back(s, index, extBase, extIndex);
  }
  return NULL;
}
