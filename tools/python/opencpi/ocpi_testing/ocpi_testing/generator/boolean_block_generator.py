#!/usr/bin/env python3

# Class for generating boolean sample input data
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Block based generator for Boolean samples."""

import random
from .base_block_generator import BaseBlockGenerator
from .boolean_generator import BooleanGeneratorDefaults


class BooleanBlockGenerator(BaseBlockGenerator):
    """Boolean protocol test data generator."""

    def __init__(self, block_length, number_of_soak_blocks=25):
        """Initialise boolean block generator class.

        Defines the default values for the variables that control the values
        and size of messages that are generated.

        Returns:
            An initialised BooleanGenerator instance.
        """
        super().__init__(block_length, number_of_soak_blocks)

        # Set variables as local as may be modified when set in the specific
        # generator. Keep the same variable names to ensure documentation
        # matches.
        self.MESSAGE_SIZE_LONGEST = BooleanGeneratorDefaults.MESSAGE_SIZE_LONGEST

    def sample(self, seed, subcase):
        """Generate sample messages to test different supported values.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)
        number_of_samples = max(self.SAMPLE_DATA_LENGTH, self._block_length)
        if subcase == "all_zero":
            return self._split_samples_into_opcodes([False] * number_of_samples)

        elif subcase == "all_maximum":
            return self._split_samples_into_opcodes([True] * number_of_samples)

        else:
            raise ValueError(f"Unexpected subcase of {subcase} for sample()")

    def _generate_random_samples(self, number_of_samples):
        """Generate samples with random values.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        return [bool(random.randint(0, 1)) for _ in range(number_of_samples)]

    def _full_scale_random_sample_values(self, number_of_samples):
        """Generate samples with random values.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        return [bool(random.randint(0, 1)) for _ in range(number_of_samples)]
