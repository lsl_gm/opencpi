#!/usr/bin/env python3

# Class for generating double sample input data
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Block based generator for Double samples."""

import random
import math
from .base_block_generator import BaseBlockGenerator
from .double_generator import DoubleGeneratorDefaults


class DoubleBlockGenerator(BaseBlockGenerator):
    """Double protocol test data generator."""

    def __init__(self, block_length, number_of_soak_blocks=25):
        """Initialise double block generator class.

        Defines the default values for the variables that control the values
        and size of messages that are generated.

        Returns:
            An initialised DoubleGenerator instance.
        """
        super().__init__(block_length, number_of_soak_blocks)

        # Set variables as local as may be modified when set in the specific
        # generator. Keep the same variable names to ensure documentation
        # matches.
        self.MESSAGE_SIZE_LONGEST = DoubleGeneratorDefaults.MESSAGE_SIZE_LONGEST
        self.DOUBLE_MINIMUM = DoubleGeneratorDefaults.DOUBLE_MINIMUM
        self.DOUBLE_MAXIMUM = DoubleGeneratorDefaults.DOUBLE_MAXIMUM
        self.DOUBLE_SMALL = DoubleGeneratorDefaults.DOUBLE_SMALL
        self.TYPICAL_AMPLITUDE_MEAN = \
            DoubleGeneratorDefaults.TYPICAL_AMPLITUDE_MEAN
        self.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH = \
            DoubleGeneratorDefaults.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH
        self.TYPICAL_MAXIMUM_AMPLITUDE = \
            DoubleGeneratorDefaults.TYPICAL_MAXIMUM_AMPLITUDE

    def sample(self, seed, subcase):
        """Generate sample messages to test different supported values.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)
        number_of_samples = max(self.SAMPLE_DATA_LENGTH, self._block_length)
        if subcase == "all_zero":
            return self._split_samples_into_opcodes([0.0] * number_of_samples)

        elif subcase == "all_maximum":
            return self._split_samples_into_opcodes([self.DOUBLE_MAXIMUM] * number_of_samples)

        elif subcase == "all_minimum":
            return self._split_samples_into_opcodes([self.DOUBLE_MINIMUM] * number_of_samples)

        elif subcase == "large_positive":
            data = [0.0] * number_of_samples
            for index in range(number_of_samples):
                data[index] = random.uniform(
                    self.TYPICAL_MAXIMUM_AMPLITUDE,
                    self.DOUBLE_MAXIMUM)
            return self._split_samples_into_opcodes(data)

        elif subcase == "large_negative":
            data = [0.0] * number_of_samples
            for index in range(number_of_samples):
                data[index] = random.uniform(
                    self.DOUBLE_MINIMUM,
                    -self.TYPICAL_MAXIMUM_AMPLITUDE)
            return self._split_samples_into_opcodes(data)

        elif subcase == "near_zero":
            data = [0.0] * number_of_samples
            for index in range(number_of_samples):
                data[index] = random.uniform(-self.SAMPLE_NEAR_RANGE,
                                             self.SAMPLE_NEAR_RANGE)
            return self._split_samples_into_opcodes(data)

        elif subcase == "positive_infinity":
            data = self._full_scale_random_sample_values(number_of_samples)
            for count in range(4):
                data[random.randint(0, number_of_samples)] = float("inf")
            return self._split_samples_into_opcodes(data)

        elif subcase == "negative_infinity":
            data = self._full_scale_random_sample_values(number_of_samples)
            for count in range(4):
                data[random.randint(0, number_of_samples)] = -float("inf")
            return self._split_samples_into_opcodes(data)

        elif subcase == "not_a_number":
            data = self._full_scale_random_sample_values(number_of_samples)
            for count in range(4):
                data[random.randint(0, number_of_samples)] = float("nan")
            return self._split_samples_into_opcodes(data)

        else:
            return super().sample(seed, subcase)

    def _generate_random_samples(self, number_of_samples):
        """Generate samples with random values.

        The values generated are a subset of the whole supported range that
        doubles can represent, this range size is set by
        self.LIMITED_SCALE_FACTOR.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        data = [0.0] * number_of_samples
        for index in range(number_of_samples):
            data[index] = random.uniform(-self.LIMITED_SCALE_FACTOR,
                                         self.LIMITED_SCALE_FACTOR) * self.DOUBLE_MAXIMUM
        return data

    def _full_scale_random_sample_values(self, number_of_samples):
        """Generate samples with random values.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        data = [0.0] * number_of_samples
        for index in range(number_of_samples):
            # Range of random.uniform() must fit in double, so for minimum
            # to maximum use -1..1 and then scale up.
            data[index] = random.uniform(-1.0, 1.0) * self.DOUBLE_MAXIMUM
        return data
