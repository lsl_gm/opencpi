#!/usr/bin/env python3

# Import testing protocol generators for different protocols
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""Protocol generators."""


from .boolean_generator import BooleanGenerator
from .boolean_block_generator import BooleanBlockGenerator
from .character_generator import CharacterGenerator
from .character_block_generator import CharacterBlockGenerator
from .short_generator import ShortGenerator
from .short_block_generator import ShortBlockGenerator
from .long_generator import LongGenerator
from .long_block_generator import LongBlockGenerator
from .long_long_generator import LongLongGenerator
from .long_long_block_generator import LongLongBlockGenerator
from .unsigned_character_generator import UnsignedCharacterGenerator
from .unsigned_character_block_generator import UnsignedCharacterBlockGenerator
from .unsigned_short_generator import UnsignedShortGenerator
from .unsigned_short_block_generator import UnsignedShortBlockGenerator
from .unsigned_long_generator import UnsignedLongGenerator
from .unsigned_long_block_generator import UnsignedLongBlockGenerator
from .unsigned_long_long_generator import UnsignedLongLongGenerator
from .unsigned_long_long_block_generator import UnsignedLongLongBlockGenerator
from .float_generator import FloatGenerator
from .float_block_generator import FloatBlockGenerator
from .double_generator import DoubleGenerator
from .double_block_generator import DoubleBlockGenerator
from .complex_character_generator import ComplexCharacterGenerator
from .complex_character_block_generator import ComplexCharacterBlockGenerator
from .complex_short_generator import ComplexShortGenerator
from .complex_short_block_generator import ComplexShortBlockGenerator
from .complex_long_generator import ComplexLongGenerator
from .complex_long_block_generator import ComplexLongBlockGenerator
from .complex_float_generator import ComplexFloatGenerator
from .complex_float_block_generator import ComplexFloatBlockGenerator
from .complex_double_generator import ComplexDoubleGenerator
from .complex_double_block_generator import ComplexDoubleBlockGenerator
