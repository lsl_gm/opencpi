#!/usr/bin/env python3

# Testing of code in complex float_generator.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Pytest for Complex Float Block Generator."""

import numpy
import random
import statistics
import unittest

from opencpi.ocpi_testing.ocpi_testing.generator.complex_float_block_generator import ComplexFloatBlockGenerator


class TestComplexFloatBlockGenerator(unittest.TestCase):
    def setUp(self):
        self.test_generator = ComplexFloatBlockGenerator(
            block_length=48, number_of_soak_blocks=10)
        self.seed = 42

    def _analyse_messages(self, messages):
        """Analyse messages from the generator.
        Counts the number of each type of message opcode so that it is easy to
        test for the existence and non-existence of opcodes in the results.
        For sample opcodes calculates the minimum and maximum values so that
        the range of generated values can be checked.
        """
        count = 0
        real_minimum = self.test_generator.FLOAT_MAXIMUM
        real_maximum = self.test_generator.FLOAT_MINIMUM
        imag_minimum = self.test_generator.FLOAT_MAXIMUM
        imag_maximum = self.test_generator.FLOAT_MINIMUM
        opcodes = {"sample": 0, "time": 0, "sample_interval": 0,
                   "flush": 0, "discontinuity": 0, "metadata": 0, "test": 0}
        for m in messages:
            self.assertTrue(m["opcode"] in opcodes)
            opcodes[m["opcode"]] += 1
            if m["opcode"] == "sample":
                self.assertGreater(len(m["data"]), 0)
                count += len(m["data"])
                real_minimum = min(real_minimum, min(
                    value.real for value in m["data"]))
                real_maximum = max(real_maximum, max(
                    value.real for value in m["data"]))
                imag_minimum = min(imag_minimum, min(
                    value.imag for value in m["data"]))
                imag_maximum = max(imag_maximum, max(
                    value.imag for value in m["data"]))
        return count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes

    def test_sample(self):
        messages = self.test_generator.sample(self.seed, "all_zero")
        count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes = self._analyse_messages(
            messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all zero
        self.assertEqual(real_minimum, 0)
        self.assertEqual(real_maximum, 0)
        self.assertEqual(imag_minimum, 0)
        self.assertEqual(imag_maximum, 0)

        messages = self.test_generator.sample(self.seed, "all_maximum")
        count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes = self._analyse_messages(
            messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all maximum
        self.assertEqual(real_minimum, self.test_generator.FLOAT_MAXIMUM)
        self.assertEqual(real_maximum, self.test_generator.FLOAT_MAXIMUM)
        self.assertEqual(imag_minimum, self.test_generator.FLOAT_MAXIMUM)
        self.assertEqual(imag_maximum, self.test_generator.FLOAT_MAXIMUM)

        messages = self.test_generator.sample(self.seed, "all_minimum")
        count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes = self._analyse_messages(
            messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all minimum
        self.assertEqual(real_minimum, self.test_generator.FLOAT_MINIMUM)
        self.assertEqual(real_maximum, self.test_generator.FLOAT_MINIMUM)
        self.assertEqual(imag_minimum, self.test_generator.FLOAT_MINIMUM)
        self.assertEqual(imag_maximum, self.test_generator.FLOAT_MINIMUM)

        messages = self.test_generator.sample(self.seed, "real_zero")
        count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes = self._analyse_messages(
            messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all minimum
        self.assertEqual(real_minimum, 0)
        self.assertEqual(real_maximum, 0)
        print(f"imag_minimum: {imag_minimum}")
        self.assertLess(imag_minimum, 0)
        self.assertGreater(imag_maximum, 0)

        messages = self.test_generator.sample(self.seed, "imaginary_zero")
        count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes = self._analyse_messages(
            messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all minimum
        self.assertLess(real_minimum, 0)
        self.assertGreater(real_maximum, 0)
        self.assertEqual(imag_minimum, 0)
        self.assertEqual(imag_maximum, 0)

        messages = self.test_generator.sample(self.seed, "large_positive")
        count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes = self._analyse_messages(
            messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all large positive
        self.assertGreaterEqual(
            real_minimum, self.test_generator.FLOAT_MAXIMUM - self.test_generator.SAMPLE_NEAR_RANGE)
        self.assertLessEqual(real_maximum, self.test_generator.FLOAT_MAXIMUM)
        self.assertGreaterEqual(
            imag_minimum, self.test_generator.FLOAT_MAXIMUM - self.test_generator.SAMPLE_NEAR_RANGE)
        self.assertLessEqual(imag_maximum, self.test_generator.FLOAT_MAXIMUM)

        messages = self.test_generator.sample(self.seed, "large_negative")
        count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes = self._analyse_messages(
            messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all large negative
        self.assertGreaterEqual(
            real_minimum, self.test_generator.FLOAT_MINIMUM)
        self.assertLessEqual(
            real_maximum, self.test_generator.FLOAT_MINIMUM + self.test_generator.SAMPLE_NEAR_RANGE)
        self.assertGreaterEqual(
            real_minimum, self.test_generator.FLOAT_MINIMUM)
        self.assertLessEqual(
            real_maximum, self.test_generator.FLOAT_MINIMUM + self.test_generator.SAMPLE_NEAR_RANGE)

        messages = self.test_generator.sample(self.seed, "near_zero")
        count, real_minimum, real_maximum, imag_minimum, imag_maximum, opcodes = self._analyse_messages(
            messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all close to zero
        self.assertGreaterEqual(
            real_minimum, -self.test_generator.SAMPLE_NEAR_RANGE)
        self.assertLessEqual(
            real_maximum, self.test_generator.SAMPLE_NEAR_RANGE)
        self.assertGreaterEqual(
            imag_minimum, -self.test_generator.SAMPLE_NEAR_RANGE)
        self.assertLessEqual(
            imag_maximum, self.test_generator.SAMPLE_NEAR_RANGE)

    def test_generate_sine_wave_samples(self):
        data = []
        for n in range(self.test_generator._block_length):
            data += self.test_generator._generate_sine_wave_samples(n, "test")

        # At least one block of data.
        self.assertGreater(len(data), self.test_generator._block_length)
        # Over enough samples the mean of the sine waves will tend towards zero.
        real_mean = statistics.mean(value.real for value in data)
        self.assertLess(
            abs(real_mean), self.test_generator.TYPICAL_AMPLITUDE_MEAN)
        imag_mean = statistics.mean(value.imag for value in data)
        self.assertLess(
            abs(imag_mean), self.test_generator.TYPICAL_AMPLITUDE_MEAN)

    def test_generate_random_samples(self):
        data = []
        for n in range(self.test_generator._block_length):
            data += self.test_generator._generate_random_samples(n)

        # At least one block of data.
        self.assertGreater(len(data), self.test_generator._block_length)
        # Check that both positive and negative values produced.
        self.assertGreater(max(value.real for value in data), 0)
        self.assertLess(min(value.real for value in data), 0)
        self.assertGreater(max(value.imag for value in data), 0)
        self.assertLess(min(value.imag for value in data), 0)

    def test_full_scale_random_sample_values(self):
        data = []
        for n in range(self.test_generator._block_length):
            data += self.test_generator._full_scale_random_sample_values(n)

        # At least one block of data.
        self.assertGreater(len(data), self.test_generator._block_length)
        # Check that both positive and negative values produced.
        self.assertGreater(max(value.real for value in data), 0)
        self.assertLess(min(value.real for value in data), 0)
        self.assertGreater(max(value.imag for value in data), 0)
        self.assertLess(min(value.imag for value in data), 0)
