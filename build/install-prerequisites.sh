#!/bin/bash --noprofile
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

#############################################################################################
# This script builds and installs any prerequisite software packages required by OpenCPI
# These are packages that are not generally/easily available for all platforms from
# network-based package repositories.
set -e
[ "$1" = -f ] && force=1 && shift
# Ensure exports
source ./scripts/init-opencpi.sh
# Ensure CDK and TOOL variables
source ./cdk/opencpi-setup.sh -e
# Ensure TARGET variables
source $OCPI_CDK_DIR/scripts/ocpitarget.sh "$1"
source $OCPI_CDK_DIR/scripts/setup-prereq-dirs.sh

# arg is where a directory of prerequisite directories might be found
function add_prereq_dirs {
  shopt -s nullglob
  for p in $1/prerequisites/*; do
    [[ -d $p && $p != *.hold && -x $p/install-$(basename $p).sh ]] && topprereqs+=" $p"
  done
}
add_prereq_dirs build
for p in project-registry/*; do
  add_prereq_dirs $p
done

$OCPI_CDK_DIR/scripts/enable-rcc-platform.sh $OCPI_TARGET_PLATFORM
echo Building/installing prerequisites for the $OCPI_TARGET_DIR platform, now running on $OCPI_TOOL_PLATFORM.
echo Building prerequisites in $OCPI_PREREQUISITES_BUILD_DIR.
echo Installing them in $OCPI_PREREQUISITES_INSTALL_DIR.

prereq_build_not_required() {
  preq=$1
  timestamp_file=$2
  if [ ! -f $timestamp_file ]; then
    return 1
  fi
  echo "Prerequisite \`$preq\` was already built for $OCPI_TARGET_DIR on $(< $timestamp_file)"
  if [ -n "$force" ]; then
    echo "Option \`-f\` provided (force), so rebuilding \`$preq\`..."
    rm $timestamp_file
    return 1
  fi
  return 0
}

find_install_dir() {
  preq=${1}
  declare -a install_dirs=($preq $(echo $preq | tr - _) $(echo $preq | tr -d -) $(echo $preq | tr -d _))
  # Try to find directory that prereq is installed into. This is not guaranteed to be the same as
  # the name of the prereq.
  # FIXME: Naming should be standardized so we're not just guessing.
  for install_dir in "${install_dirs[@]}" ; do
    if [[ -d $OCPI_PREREQUISITES_DIR/$install_dir ]] ; then
      echo $OCPI_PREREQUISITES_DIR/$install_dir
      return 0
    fi
  done
  echo ""
}

if [ -n "$OCPI_TARGET_PREREQUISITES" ]; then
  echo -------------------------------------------------------------------------------------------
  echo "Before building/installing common OpenCPI prerequisites, there are $OCPI_TARGET_PLATFORM-specific ones: $OCPI_TARGET_PREREQUISITES"
  # First build the target-specific script for the TOOL platform

  for p in $OCPI_TARGET_PREREQUISITES; do
    read preq tool <<<$(echo $p | tr : ' ')
    timestamp_file=$OCPI_PREREQUISITES_DIR/$preq/built-timestamp-$OCPI_TARGET_DIR
    if prereq_build_not_required $preq $timestamp_file; then
      continue
    fi
    script=$OCPI_TARGET_PLATFORM_DIR/install-$preq.sh
    [ -x $script ] || (echo No executable installation script found in $script. && exit 1)
    if [ -z "$tool" -o "$tool" = $OCPI_TOOL_PLATFORM ]; then
      echo --- Building/installing the $OCPI_TARGET_PLATFORM-specific prerequisite $preq for executing on $OCPI_TOOL_PLATFORM.
      (for e in $(env | grep OCPI_TARGET_|sed 's/=.*//'); do unset $e; done &&
        $script $OCPI_TOOL_PLATFORM)
      date > $timestamp_file
    fi
    if [ -z "$tool" -o "$tool" = $OCPI_TARGET_PLATFORM ]; then
      echo --- Building/installing the $OCPI_TARGET_PLATFORM-specific prerequisite $preq for executing on $OCPI_TARGET_PLATFORM.
      $script $OCPI_TARGET_PLATFORM
      date > $timestamp_file
    fi
    if [ -n "$tool" -a "$tool" != $OCPI_TOOL_PLATFORM -a "$tool" != $OCPI_TARGET_PLATFORM ] ; then
      echo --- Skipping the prequisite \"$p\" for $OCPI_TARGET_PLATFORM since we are running on $tool.
    fi
  done
fi
echo -------------------------------------------------------------------------------------------
echo "Now installing the generic (for all platforms) prerequisites for $OCPI_TARGET_PLATFORM."
for p in $topprereqs; do
  echo -------------------------------------------------------------------------------------------
  preq=$(basename $p)
  install_dir=$(find_install_dir $preq)
  timestamp_file=$install_dir/built-timestamp-$OCPI_TARGET_DIR
  if prereq_build_not_required $preq $timestamp_file; then
    continue
  fi
  script=$p/install-$preq.sh
  if [ -x $script ] ; then
    $script $1
    install_dir=$(find_install_dir $preq)
    timestamp_file=$install_dir/built-timestamp-$OCPI_TARGET_DIR
    if [[ -d $install_dir ]] ; then date > $timestamp_file ; fi
  else
    echo "The installation script for \`$preq\` ($script) is missing or not executable." >&2
    exit 1
  fi
done
echo -------------------------------------------------------------------------------------------
echo All these OpenCPI prerequisites have been successfully installed for $OCPI_TARGET_PLATFORM:
printf "    "
for p in $topprereqs; do printf " "$(basename $p); done
echo
