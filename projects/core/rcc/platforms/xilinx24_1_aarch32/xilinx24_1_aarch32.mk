OcpiXilinxLinuxRepoTag:=xilinx-v2024.1

mkfile_dir:=$(dir $(realpath $(lastword $(MAKEFILE_LIST))))

include $(OCPI_CDK_DIR)/include/xilinx/xilinx-rcc-platform-definition.mk

OcpiCXXFlags+="-Wno-type-limits"
OcpiKernelDir=$(mkfile_dir)/gen/kernel-artifacts/aarch32/headers
OcpiPlatformOs:=linux
OcpiPlatformOsVersion:=24_1
OcpiPlatformArch:=aarch32
OcpiRequiredCXXFlags:=$(OcpiRequiredCXXFlags) -Wno-psabi

