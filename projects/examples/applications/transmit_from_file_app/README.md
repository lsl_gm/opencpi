## TRANSMIT_FROM_FILE_APP

![Figure 1. HDL Component Assembly](./fig/transmit_path.svg)

At this time, the only supported OpenCPI hardware platforms are: e31x, zed, zcu104, and plutosdr_csts

NOTE: The following instructions assume that the user has successfully installed the OpenCPI framework. It also assumes that the user has successfully installed the desired OpenCPI supported platforms.
For example purposes, only the ZedBoard platform will be shown but similar execution commands may replace the ``zed`` platform with ``e31x``, ``zcu104`` or ``plutosdr``. Similarly, the ``ubuntu22_04`` platform will be demonstrated for host-based components but could be any supported OpenCPI host platform.

Note: The ZedBoard should be paired with an Analog Devices FMCOMMS2, FMCOMMS3, or FMCOMMS4 daughter board.

Install and Build SDR Components
---
Install and build the SDR components if not already done so.
```sh
        $ cd ~/opencpi/projects/comps
        $ git submodule update --init --remote ocpi.comp.sdr
        $ cd ocpi.comp.sdr
        $ ocpidev register project
        $ ocpidev build -d hdl/primitives --hdl-platform zed
        $ ocpidev build --rcc-platform ubuntu22_04
```
  Note: Only the primtives need to be built at this point for HDL components.

Build Example Components
---
The transmit to file application allows for input sample files in the format of short, float, complex short, or complex float to be read from a file. The examples project uses RCC components to perform this function. Each will convert from the input file type to the required complex short type for processing. Build desired component based on your input file type:
```sh
        $ cd ~/opencpi/projects/examples/
        $ ocpidev register project
        $ ocpidev build --rcc-platform ubuntu22_04
```
Build Digital Radio Controller
---
```sh
	$ cd ~/opencpi/projects/examples/hdl/devices/drc_fmcomms.rcc/
	$ ocpidev build --rcc-platform ubuntu22_04
```
Build Assembly
---
```sh
	$ cd ~/opencpi/projects/examples/hdl/assemblies/transmit_from_file_app/
 	$ ocpidev build --hdl-platform zed --workers-as-needed
```
Build Application
---
```sh 
	$ ~/opencpi/projects/examples/applications/transmit_from_file_app
	$ ocpidev build --rcc-platform ubuntu22_04
```
Platform Preparation
---
The PlutoSDR has a default IP address of 192.168.2.1 and can be used immediately once the platform is booted. The ZedBoard and E310 do not have a default IP address and must be set prior to use. We will continue to use the `zed` platform as an example but approach must be used for both E310 and PlutoSDR. We will use the following to indicate the host and remote terminal prompts:
```
	(local host) :  ~$
	(remote platform): ~# 
```
Power up ZedBoard with USB connection to local host. The interface will usually register as a device with tag /dev/ttyACM0. The E310 is usually /dev/ttyUSB0. The zcu104 will have up to 4 device identifiers. Typically, the zcu104 will use ttyUSB1 for user access. Use a serial interface application such as ``screen`` to access the platform device. Both the default login and password for ZedBoard and E310 is: ``root``. The login and password for PlutoSDR is ``root`` and ``analog``.
```sh
        ~$ sudo screen /dev/ttyACM0 115200
```
Once you have logged in, set the IP address and exit the serial interface.
```sh
        ~# ifconfig eth0 192.168.2.2
        ~# Ctrl-a  d
```
The platform IP address must also reside on the same subnet as the host socket.

Set OCPI_SERVER_ADDRESSES environment variable to match ZedBoard IP address and set port to 12345.
```sh
        ~$ export OCPI_SERVER_ADDRESSES=192.168.2.2:12345
```
Determine the associated network interface name and set as the opencpi socket interface. As an example, we use enx001.
```sh
        ~$ export OCPI_SOCKET_INTERFACE=enx001
```
The chosen platform IP address must reside on the same subnet as the host socket. If not, change platform IP of local host to match subnet. For example:
```sh
	~$ ifconfig enx001 192.168.2.10
```
From the host, load the platform sandbox and perform remote start.
```sh
        ~$ ocpiremote load -w zed -s xilinx24_1_aarch32 -p root
        ~$ ocpiremote start -b -p root
```
Verify platform.
```sh
        ~$ ocpirun -C
```
If successfully started, then the following output will be displayed on host:

![Figure 2. ACtive OpenCPI Platforms](./fig/ocpirun-C.png)

Execute Application
---
The ACI based executable initiates the transmit_from_file_app.xml application. The executable file is located in the platform build directory but executed from the transmit_from_file_app directory. 

Ensure that the artifact libraries required to exeecute application are in the OCPI_LIBRARY_PATH:
```sh
        $ export OCPI_LIBRARY_PATH=~/opencpi/projects/core/artifacts/:~/opencpi/projects/
comps/ocpi.comp.sdr/artifacts/:~/opencpi/projects/examples/artifacts/
```
If running default options, then execute the following:
```sh
        $ cd ~/opencpi/projects/examples/applications/transmit_from_file_app/
        $ ./target-ubuntu22_04/transmit_from_file_app
```
To view available options, execute the application with the --help argument
```sh
        $ ./target-ubuntu22_04/transmit_from_file_app --help
```
NOTE: If running in Rocky 8 or 9, then you may need to turn off or configure firewall.
```sh
	$ sudo systemctl stop firewalld.service
```
NOTE: To improve stability of transmit signal, it is recommeded that the application be run in the ``standalone`` mode. This would require building the drc and application for the platform's embedded processor (e.g. xilinx24_1_aarch32) and porting all required files (application, artifacts, signal, and required libraries) to the respective platform folder. The OpenCPI Installation guide provides further instruction to prepare the platform for ``standalone`` mode.

```sh
Options:
 --help                Display options
 --display             Show final configuration properties
 --verbose             Show ocpirun debug
 --rcc-platform        Defines which RCC platform to run file_read RCC component, 
                       [default = rcc0]
 --tx-freq             Transmit carrier frequency (MHz), [default = 2450.0]
 --tx-offset           Transmit frequency offset (MHz), [default = 0.0]
 --tx-filename         Name of samples file, [default = signals/psk_signal.sc16]
 --file-type           Format of input sample file (short, float32, complex short(sc16),
                       or complex float(fc32)), [default = sc16]
 --repeat              Repeat transmit of samples from file when EOF [default = false]
 --tx-rate             Transmit sample rate (Msps), [default = 0.25]
 --tx-bandwidth        Transmit 3dB bandwidth (MHz), [default = 0.25]
 --tx-gain             Transmit gain = gain_dB (dB), [default = -10.0]
 --timeout             Duration to run application (seconds), [default = -1] (no timeout)
`
