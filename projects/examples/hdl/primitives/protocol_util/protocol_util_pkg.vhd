-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.
library ieee; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi;
package protocol_util is

    component nonsample_passthrough_marshaller is
        generic(
            INPUT_DATA_WIDTH  : positive := 32; -- default to 32
            OUTPUT_DATA_WIDTH : positive := 32; -- default to 32
            WSI_MBYTEEN_WIDTH : positive);
        port(
            clk       : in  std_logic;
            rst       : in  std_logic;
            -- INPUT
            in_data         : in  std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
            in_valid        : in  std_logic;
            in_ready        : in  std_logic;
            in_som          : in  std_logic;
            in_eom          : in  std_logic;
            in_opcode       : in  natural;
            in_eof          : in  std_logic;
            in_take         : out std_logic;
            -- OUTPUT TO WORKER FROM INPUT SIDE
            samp_reset      : out std_logic;
            samp_data_in    : out std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
            samp_in_vld     : out std_logic;
            samp_som_in     : out std_logic;
            samp_eom_in     : out std_logic;
            oeof            : out std_logic;
            take            : in  std_logic;
            -- INPUT FROM WORKER TO OUTPUT SIDE
            samp_data_out   : in  std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0); -- Separate port for samples data
            samp_out_vld    : in  std_logic;
            samp_eom_out    : in  std_logic;
            ieof            : in  std_logic;
            enable          : out std_logic;
            -- OUTPUT
            out_data        : out std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0);
            out_valid       : out std_logic;
            out_byte_en     : out std_logic_vector(WSI_MBYTEEN_WIDTH-1 downto 0);
            out_give        : out std_logic;
            out_eom         : out std_logic;
            out_opcode      : out natural;
            out_eof         : out std_logic;
            out_ready       : in  std_logic;
            -- DEBUG
            passthrough     : in std_logic := '0' -- Bypass worker
        );
    end component;

end package protocol_util;
