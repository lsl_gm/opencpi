.. discontinuity_gate_b RCC worker


.. _discontinuity_gate_b-RCC-worker:


``discontinuity_gate_b.rcc`` RCC Worker
=======================================
This component is only implemented as an RCC worker.

Detail
------
.. ocpi_documentation_worker::

.. Skeleton comment: If not a HDL worker / implementation then the below
   section and directive should be deleted. This comment should be removed in
   the final version of this page.

Utilization
-----------
.. ocpi_documentation_utilization::
