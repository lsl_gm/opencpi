#!/usr/bin/env python3

"""
Use this file to verify your input data.
Args: <input-file>
"""
import struct
import shutil
import numpy as np
import sys
import os.path

msg_sizes = { "08" : 4,
              "10" : 3,
              "16" : 6,
              "24" : 14,
              "32" : 15 }

crc_variations = {"08a" : "CRC-8",
                  "08b" : "CRC-8/EBU",
                  "10"  : "CRC-10",
                  "16a" : "CRC-16/CCITT-FALSE",
                  "16b" : "CRC-16/BUYPASS",
                  "16c" : "CRC-16/ARC",
                  "16d" : "Zigbee with KERMIT",
                  "24"  : "CRC-24/MODE-S",
                  "32a" : "CRC-32/BZIP2",
                  "32b" : "CRC-32",
                  "32c" : "CRC-32C"}
                
packet_data =  {# 4 message bytes with 3 data bytes and 1 CRC byte
                # CRC-8
                "08a" : np.uint8([0x8D, 0x48, 0x40, 0xAE]),
                # CRC-8/EBU
                "08b" : np.uint8([0x8D, 0x48, 0x40, 0xFE]),

                # 1 message byte with 11 data bytes and 10 CRC bits
                # CRC-10
                "10" : np.uint8([0x8D, 0x02, 0x23]),

                # 6 message bytes with 4 data bytes and 2 CRC bytes
                # CRC-16/CCITT-FALSE 
                "16a" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x72, 0x05]),
                # CRC-16/BUYPASS
                "16b" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0xE3, 0x6D]),
                # CRC-16/ARC
                "16c" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x24, 0x1B]),
                # Zigbee with CRC-16/KERMIT
                "16d" : np.uint8([0x61, 0x88, 0x7f, 0x3c, 0x92, 0xc7]),

                # 14 message bytes with 11 data bytes and 3 CRC bytes
                "24" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x20, 0x2C, 0xC3, 0x71, 0xC3, 0x2C, 0xE0, 0x57, 0x60, 0x98]),
                
                # 15 message bytes with 11 data bytes and 4 CRC bytes
                # CRC-32/BZIP2
                "32a" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x20, 0x2C, 0xC3, 0x71, 0xC3, 0x2C, 0xE0, 0xEF, 0x95, 0x79, 0xD0]),
                # CRC-32
                "32b" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x20, 0x2C, 0xC3, 0x71, 0xC3, 0x2C, 0xE0, 0x8C, 0x48, 0xFA, 0x50]),
                # CRC-32C
                "32c" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x20, 0x2C, 0xC3, 0x71, 0xC3, 0x2C, 0xE0, 0x49, 0xAE, 0x60, 0x4B]) 
                }


def checkLength(size, expected_length):
    if size != expected_length:
        print("Error: Length dout = " + str(size) + " while expected length is = " + str(expected_length))
        sys.exit(1)

def checkData(filename, crc, crc_size):
    global log
    size = msg_sizes[crc_size]
        
    ofile = open(filename, 'rb')
    dout = np.fromfile(ofile, dtype=np.uint8, count=-1)
    checkLength(len(dout), size)

    data = packet_data[crc]
    failcount = 0
    for i in range(0,len(dout)):
        expected = data[i]
        received = dout[i]
        if expected != received:
            log+="\tMismatch: Expected: " + str(expected) + "\t Received: " + str(received) + "\n"
            failcount = failcount+1

    if (failcount > 0):
        print("\t***" + str(failcount) + "/" + str(len(dout)) + " value(s) do not match.")
        sys.exit(1)

    ofile.close() 

def verify(argv):

    if len(argv) < 2:
        print("Exit: Enter the  CRC bits and type (e.g. 08a for CRC-8)")
        sys.exit(1)
    if len(argv) < 3:
        print("Exit: Enter an input filename")
        sys.exit(1)

    crc_variation = argv[1]
    crc_size = crc_variation[0:2]

    filename = argv[2]   
    
    crc_bits = np.uint8(os.environ.get("OCPI_TEST_crcBits"))
    crc_poly  = np.uint64(os.environ.get("OCPI_TEST_crcKey"))
    #init_crc = np.uint64(os.environ.get("OCPI_TEST_initial_crc"))
    #xor_crc  = np.uint64(os.environ.get("OCPI_TEST_xor_crc"))
    #ref_input  = np.bool(os.environ.get("OCPI_TEST_reflect_input"))
    #ref_crc  = np.bool(os.environ.get("OCPI_TEST_reflect_crc"))
    #reverse_crc  = np.bool(os.environ.get("OCPI_TEST_reverse_crc"))
    
    
    try:
        checkData(filename, crc_variation, crc_size)
    except: 
        print("\t***" + crc_variations[crc_variation])
        print("\tCRC Length: " + str(crc_bits) + " bits")
        print("\tCRC Key: " + hex(crc_poly))
        #print("\tInitial CRC: " + hex(init_crc))
        #print("\tXOR CRC: " + hex(xor_crc))
        #print("\tReflect Input: " + str(ref_input))
        #print("\tReflect CRC: " + str(ref_crc))
        #print("\tReverse CRC: " + str(reverse_crc))
        #print("Couldn't finish checkData()")
        sys.exit(1)

def main():
    verify(sys.argv)

if __name__ == '__main__':
    main()
