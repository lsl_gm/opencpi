.. convert_xf_xs RCC worker


.. _convert_xf_xs-RCC-worker:


``convert_xf_xs.rcc`` RCC Worker
====================================
Converts input samples from protocol 'complex_float_timed_sample-prot' to 'complex_short_timed_sample-prot'.
Scales the float by 2^15 to integer value.

Detail
------
.. ocpi_documentation_worker::

.. Skeleton comment: If not a HDL worker / implementation then the below
   section and directive should be deleted. This comment should be removed in
   the final version of this page.

Utilization
-----------
.. ocpi_documentation_utilization::
