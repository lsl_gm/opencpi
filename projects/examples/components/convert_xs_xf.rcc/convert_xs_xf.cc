// Convert complex-short to complex-float RCC worker
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "convert_xs_xf-worker.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Convert_xs_xfWorkerTypes;

class Convert_xs_xfWorker : public Convert_xs_xfWorkerBase {


  void convert_data(const Complex_short_timed_sampleSampleData *in_data,
		  Complex_float_timed_sampleSampleData *out_data,
		  unsigned int in_data_length) 
  {
	  for (size_t i=0; i < in_data_length; i++) {
		float f_real = float(in_data[i].real);
		float f_imag = float(in_data[i].imaginary);
		float f_real_scaled = (f_real / (32768.));
		float f_imag_scaled = (f_imag / (32768.)); 

		out_data[i].real = f_real_scaled;
		out_data[i].imaginary = f_imag_scaled; 
	  }
  }

  RCCResult run(bool /*timedout*/) 
  {
    switch (input.opCode())
    {
	    case Complex_short_timed_sampleSample_OPERATION:
	    {
		    const Complex_short_timed_sampleSampleData* inData = input.sample().data().data();
		    Complex_float_timed_sampleSampleData* outData = output.sample().data().data();
		    unsigned int num_samples = input.sample().data().size();

		    output.setOpCode(Complex_float_timed_sampleSample_OPERATION);
		    output.sample().data().resize(num_samples);
		    convert_data(inData, outData, num_samples);
	    }
	    break;

	    default:
	    {
	    }
	    break;
    }
    return RCC_ADVANCE;
 } 
};

CONVERT_XS_XF_START_INFO
CONVERT_XS_XF_END_INFO
