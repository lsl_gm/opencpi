-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity wordPack_impl is
Generic (    
   dataWidth     : integer;
   CRCdataWidth  : integer;
   numInWords    : integer );
Port (
    clk                 : in std_logic;
    reset               : in std_logic;
    input               : in std_logic_vector((dataWidth - 1) downto 0);
    vld_in              : in std_logic;
    output              : out std_logic_vector((CRCdataWidth - 1) downto 0);
    vld_out             : out std_logic;
    packStart           : in std_logic;
    packDone            : out std_logic );
end wordPack_impl;

architecture Behavioral of wordPack_impl is

    attribute mark_debug : string;

    signal  wordCtr     : unsigned(7 downto 0) := x"00"; -- 8-bit word counter
    attribute mark_debug of wordCtr : signal is "TRUE";
    signal  buf         : std_logic_vector((CRCdataWidth - 1) downto 0) := (others => '0');
    signal  vld_flag    : std_logic;

begin

process(clk) 

    variable upperBit       : integer := 0;
    variable lowerBit       : integer := 0;

begin
        if (rising_edge(clk)) then
        if (reset = '1') then
            buf <= (others => '0');
            wordCtr <= (others => '0');
            vld_flag <= '0';
            packDone <= '0';
        elsif (packStart = '1') then
             if (vld_in = '1') then
                if ( wordCtr < (numInWords - 1) ) then
                           buf(((CRCdataWidth - 1) - to_integer(wordCtr * dataWidth)) downto 
                           (((CRCdataWidth - 1) - to_integer(wordCtr * dataWidth)) - (dataWidth - 1))) <= input((dataWidth - 1) downto 0);
                        vld_flag <= '0';
                        wordCtr <= wordCtr + 1;                 
                 else
                        buf(((CRCdataWidth - 1) - to_integer(wordCtr * dataWidth)) downto 
                                (((CRCdataWidth - 1) - to_integer(wordCtr * dataWidth)) - (dataWidth - 1))) <= input((dataWidth - 1) downto 0);
                        vld_flag <= '1';
                        wordCtr <= (others => '0');
                        packDone <= '1';
                end if;
              end if;        
         else
              packDone <= '0';
        end if;
    end if;
end process;

    vld_out <= vld_flag;
    output <= buf;

end Behavioral;
