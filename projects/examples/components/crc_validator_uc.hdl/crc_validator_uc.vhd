-- crc_validator_uc HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all;

architecture OCPI_Wrapper of worker is

component crc_validator_impl
 Generic (
    dataWidth       : integer;          -- OCPI Parameter          
    messageWidth    : integer;          -- OCPI Parameter      
    CRCdataWidth    : integer;          -- OCPI Parameter     
    CRCbitWidth     : integer;          -- OCPI Parameter      
    numInWords      : integer;          -- OCPI Parameter      
    numOutWords     : integer;          -- OCPI Parameter      
    polyValue       : integer );        -- OCPI Parameter   
  Port ( 
        clk         : in std_logic;
        reset       : in std_logic;
        data        : in std_logic_vector((dataWidth - 1) downto 0);
        valid       : in std_logic;
        eof         : in std_logic;
        take        : out std_logic;
        message     : out std_logic_vector((messageWidth - 1) downto 0);
        message_vld : out std_logic;
        message_rdy : in std_logic;
        message_som : out std_logic;
        message_eom : out std_logic;
        message_eof : out std_logic;
        crcMessages : out std_logic_vector(31 downto 0) );
end component;

    constant CRCdataWidth       : integer := to_integer(dataBytes) * 8;
    constant CRCbitWidth        : integer := to_integer(crcBits);
    constant out_eof_delay      : integer := 16;

    signal data_crc_impl        : std_logic_vector((to_integer(dataWidth) - 1) downto 0);
    signal valid_crc_impl       : std_logic;
    signal eof_crc_impl         : std_logic;
    signal take_crc_impl        : std_logic;
    signal message_crc_impl     : std_logic_vector((to_integer(messageWidth) - 1) downto 0);
    signal message_vld_crc_impl : std_logic;
    signal message_rdy_crc_impl : std_logic;
    signal message_som_crc_impl : std_logic;
    signal message_eom_crc_impl : std_logic;
    signal crcMessages_crc_impl : std_logic_vector(31 downto 0);
    signal message_eof_crc_impl : std_logic;
    signal input_register_eof   : std_logic_vector(out_eof_delay -1  downto 0) := (others => '0');    

begin

work_impl: crc_validator_impl
generic map(
        dataWidth    => to_integer(dataWidth),       
        messageWidth => to_integer(messageWidth),
        CRCdataWidth => CRCdataWidth,           
        CRCbitWidth  => CRCbitWidth,            
        numInWords   => to_integer(dataPkts),            
        numOutWords  => to_integer(messagePkts),            
        polyValue    => to_integer(crcKey) )    
port map ( 
        clk          => ctl_in.clk,
        reset        => ctl_in.reset,
        data         => data_crc_impl,
        valid        => valid_crc_impl,
        eof          => eof_crc_impl,
        take         => take_crc_impl,
        message      => message_crc_impl,
        message_vld  => message_vld_crc_impl,
        message_rdy  => message_rdy_crc_impl,
        message_som  => message_som_crc_impl,
        message_eom  => message_eom_crc_impl,
        message_eof  => message_eof_crc_impl,
        crcMessages  => crcMessages_crc_impl );

process (ctl_in.clk)

begin

   output_out.opcode <= uchar_timed_sample_sample_op_e;

   -- Delay input EOF
   input_register_eof <= input_register_eof(out_eof_delay-2 downto 0) & input_in.eof;

   -- Worker Properties
   props_out.crcMessages <= to_ulong(crcMessages_crc_impl);

   -- Input Port Connections
   data_crc_impl <= input_in.data;
   valid_crc_impl <= input_in.valid;
   eof_crc_impl <= input_register_eof(input_register_eof'high);
   input_out.take <= input_in.valid and take_crc_impl;

   -- Output Port Connections
   output_out.data <= message_crc_impl;
   output_out.valid <= message_vld_crc_impl;
   message_rdy_crc_impl <= output_in.ready;

   -- Output Port Metadata
   output_out.som <= message_som_crc_impl;
   output_out.eom <= message_eom_crc_impl;
   output_out.eof <= message_eof_crc_impl;

end process;

end OCPI_Wrapper;
