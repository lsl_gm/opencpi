.. discontinuity_gate_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _discontinuity_gate_b:


Binary Data Flow Gate (``discontinuity_gate_b``)
================================================
Passes input binary samples to output port when discontinuity detected on ‘input_start’ port and discards input samples when discontinuity detected on ‘input_close’ port. 

Design
------
The ```discontinuity_gate_b``` component has two input ports and one output port. It is designed to pass through binary data received on the ‘input_open’ port to the output port once a discontinuity opcode is detected on the ‘input_open’ port. Until a discontinuity message is received on this port, all incoming binary data is discarded. If the gate is in an open state, samples will continue to flow until a discontinuity message is received on the ‘input_close’ port. All data samples and opcode messages received on the ‘input_close’ port are discarded. Once a discontinuity message is received on this port, all data samples received on the ‘input_open’ port are discarded, however, all opcode messages except discontinuity are allowed to pass.   
A block diagram representation of the component function is given in :numref:`discontinuity_gate_b-diagram`.

.. _discontinuity_gate_b-diagram:

.. figure:: discontinuity_gate_b.svg
   :alt: Skeleton alternative text.
   :align: center

   Discontinuity Gate Component Usage.

Interface
---------
.. literalinclude:: ../specs/discontinuity_gate_b-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
All opcodes except 'discontinuity' are passed from input_open port to output when they are received.  Opcodes received on the input_close port are discarded.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Ports
~~~~~
.. ocpi_documentation_ports::

   input_open: Primary input samples port and header detect.
   input_close: Tail detect.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../discontinuity_gate_b.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

Limitations
-----------
Limitations of ``discontinuity_gate_b`` are:

 * Must be used with components that generate discontinuity opcodes to trigger an 'open' or 'close' gate event.

Testing
-------
.. ocpi_documentation_test_platforms::

.. Removed ocpi_documentation_test_result_summary directive until it is functional
