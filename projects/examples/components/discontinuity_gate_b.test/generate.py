#!/usr/bin/env python3
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

"""
Generate binary data file used for content of packet.  
Args: <list-of-user-defined-args> <input-file>

Used to test discontinuity_gate_b.rcc component that will pass data upon
receiving a 'discontinuity' opcode on input_open port and cease passing
data when 'discontinuity' opcode received on input_close port.
The output of this test will produce an output file that when examined, will
have only the generated data Payload bytes.

Generates input file for each discontinuity gate port (input_open and 
input_close.   

input_open port:
    random bytes --> discontinuity opcode --> payload --> random bytes

input_close port:
    random_bytes --> payload --> discontinuity opcode --> random bytes

"""
import struct
import shutil
import numpy as np
import sys
import os.path
import math

def generate(argv):

    if len(argv) < 3:
        argv[1] = 32768
        return
    elif len(argv) < 4:
        print("Exit: Enter an input filename")
        return
    
    # Assign input arguments
    num_samples = int(argv[1]) # Total number of requested bytes for payload
    port = argv[2]             # Port: 'open' or 'close'
    filename = argv[3]
    
    # Generate OPCODE message bytes 
    sample_opcode = 0;
    sample_opcode_bytes = sample_opcode.to_bytes(4,'little')
    discontinuity_opcode = 4;
    discontinuity_opcode_bytes = discontinuity_opcode.to_bytes(4,'little')
    discontinuity_length = 0;
    discontinuity_length_bytes = discontinuity_length.to_bytes(4,'little');

    # Generate random bytes before and after pass-through sample data
    random_message_length = 1000
    random_bytes = np.random.randint(0,255,random_message_length, dtype=np.uint8)
    random_message_length_bytes = random_message_length.to_bytes(4,'little')
    
    # Create an all 0xAA bytes array of length of max buffer (2048) bytes
    max_buffer_size = 2048
    out_data = np.full(max_buffer_size,0xAA, dtype=np.uint8)
    num_max_buffer_bytes = max_buffer_size.to_bytes(4,'little');

    # Open data output file
    f = open(filename, 'wb')

    # Add random sample bytes to file
    f.write(random_message_length_bytes)
    f.write(sample_opcode_bytes)
    f.write(random_bytes)

    #Add OPEN Gate discontinuity OpCode to file
    if port == 'open':
        f.write(discontinuity_length_bytes)
        f.write(discontinuity_opcode_bytes)
   
    # Calculate number of required sample buffers.  Max buffer size = 2048
    num_max_buffers = math.floor(num_samples/max_buffer_size)
    size_partial_buffer = num_samples - num_max_buffers * max_buffer_size

    #Add FULL range of samples (2048 byte) to file
    for i in range(0,num_max_buffers):
        f.write(num_max_buffer_bytes)
        f.write(sample_opcode_bytes)
        for j in range(0,max_buffer_size):
            f.write(out_data[j])
    
    #Add remaining bytes for PARTIAL range of samples to file
    num_partial_bytes = size_partial_buffer.to_bytes(4,'little');
    f.write(num_partial_bytes)
    f.write(sample_opcode_bytes)
    for i in range(0,size_partial_buffer):
        f.write(out_data[i])

    #Add CLOSE Gate discontinuity OpCode to file
    if port == 'close':
        f.write(discontinuity_length_bytes)
        f.write(discontinuity_opcode_bytes)
    
    # Add post-random sample bytes to file
    f.write(random_message_length_bytes)
    f.write(sample_opcode_bytes)
    f.write(random_bytes)
    
    # Close output file
    f.close()

    print ("\tOutput filename: ", filename)
    print ("\tNumber of samples: ", num_samples)

def main():
    generate(sys.argv)

if __name__ == '__main__':
    main()
