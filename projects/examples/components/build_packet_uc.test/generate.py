#!/usr/bin/env python3
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

"""
Generate binary data file used for content of packet.  
Args: <list-of-user-defined-args> <input-file>

Used to test build_packet_b.rcc component that will add HEADER and TAIL to data.
The output of this test will produce an output file that when examined, will
have the HEADER bytes preceding the generated data bytes of the output file
followed by the TAIL bytes. 
"""
import struct
import shutil
import numpy as np
import sys
import os.path

def generate(argv):

    if len(argv) < 2:
        argv[1] = 32768
        return
    elif len(argv) < 3:
        print("Exit: Enter an input filename")
        return
    filename = argv[2]
    num_samples = int(argv[1])

    #Create an input file with all 0xAA bytes of length num_samples
    out_data = np.full(num_samples,0xAA, dtype=np.uint8)

    #Save data file
    f = open(filename, 'wb')
    for i in range(0,num_samples):
        f.write(out_data[i])
    f.close()

    print ("\tOutput filename: ", filename)
    print ("\tNumber of samples: ", num_samples)

def main():
    generate(sys.argv)

if __name__ == '__main__':
    main()

