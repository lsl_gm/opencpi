#!/usr/bin/env python3

"""
Validate odata for Packet Build (binary data file).

Tests that the output file contains proper HEADER and TAIL bytes.
Will also validate size of packet and that all data bytes are
as expected per input file.
"""
import numpy as np, sys, os.path, struct
import sys

class color:
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

def validation(argv):
    header_len = 8
    tail_len = 8
    payload_len = int(argv[1])
    sync_len=int(os.environ.get("OCPI_TEST_num_of_sync_bytes"))
    outfile = argv[2]
    header=np.uint64(os.environ.get("OCPI_TEST_header_bytes"))
    tail=np.uint64(os.environ.get("OCPI_TEST_tail_bytes"))
    
    # Read in test results outfile
    with open(outfile,"rb") as file:
        result_bytes = file.read()
    
    #***********************
    # Verify SYNC Bytes
    #***********************
    result_fail = 0
    for i in range(sync_len):
        if result_bytes[i] != 85:
            print (color.RED + color.BOLD + 'FAILED: Sync Bytes' + color.END)
            sys.exit(1)

    #***********************
    # Verify HEADER Bytes
    #***********************

    header_bytes = struct.pack('>Q',header)
    for i in range(header_len):
        if result_bytes[sync_len + i] != header_bytes[i]:
            print (color.RED + color.BOLD + 'FAILED: Header Bytes' + color.END)
            sys.exit(1)

    #***********************
    # Verify Payload Bytes
    #***********************
    payload_start = sync_len + header_len
    payload_stop = sync_len + header_len + payload_len - 1

    for i in range(payload_start, payload_stop, 1):
        if result_bytes[i] != 170:
            print (color.RED + color.BOLD + 'FAILED: Payload Bytes' + color.END)
            sys.exit(1)

    #***********************
    # Verify Tail Bytes
    #***********************
    tail_bytes = struct.pack('>Q',tail)
    for i in range(tail_len):
        if result_bytes[sync_len + header_len + payload_len + i] != tail_bytes[i]:
            print (color.RED + color.BOLD + 'FAILED: Tail Bytes' + color.END)
            sys.exit(1)

    #***********************
    # Calculate total bytes 
    #***********************
    expected_len = sync_len + header_len + payload_len + tail_len + sync_len
    outfile_len = len(result_bytes)
    if expected_len != outfile_len:
        print (color.RED + color.BOLD + 'FAILED: Byte Size Mismatch' + color.END)
        sys.exit(1)

def main():
    print ("\n"+"*"*80)
    print ("*** Python: Build simple Packet ***")
    validation(sys.argv)
    

if __name__ == '__main__':
       main()

