library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;
-- use ieee.math_real.all;

entity preambleDet_impl is
    Generic (
        sampWidth       : integer;
        samp_per_symbol : integer);
    Port (
        clk                 : in std_logic;
        reset               : in std_logic;
        smp_in              : in std_logic_vector((sampWidth - 1) downto 0);
        smp_in_vld          : in std_logic;
        smp_in_eof          : in std_logic;
        avg_pwr             : in std_logic_vector((sampWidth - 1) downto 0);
        smp_out             : out std_logic_vector((sampWidth - 1) downto 0);
        smp_vld             : out std_logic;
        smp_rdy             : in std_logic;
        smp_som             : out std_logic;
        smp_eom             : out std_logic;
        smp_eof             : out std_logic;
        preambleThreshold   : in integer);
end preambleDet_impl;

architecture Behavioral of preambleDet_impl is

    -- Set samples per pulse
    constant samp_per_pulse : integer := samp_per_symbol/2;

    -- Number of pulses in Mode S packet
    constant PKT_SIZE : integer := 240;

    constant messageWidth : integer := PKT_SIZE * samp_per_pulse;

    -- Preamble pulse values
    constant preamble_bits : integer := 16;

    -- Preamble pulse values for correlation
    constant PREAMBLE_ARRAY : std_logic_vector(preamble_bits-1 downto 0) := "1010000101000000";

    -- Set samples per 8 bit preamble
    constant samp_per_preamble : integer := integer(preamble_bits * samp_per_pulse);

    constant packet_size : integer := (messageWidth - samp_per_preamble);

    function get_preamble_array return std_logic_vector is
        variable samp_per_pulse_preamble : std_logic_vector(samp_per_preamble-1 downto 0);
    begin

        for I in 1 to preamble_bits loop
            for J in 1 to samp_per_pulse loop
                samp_per_pulse_preamble((I-1)*samp_per_pulse+(J-1)) := PREAMBLE_ARRAY(I-1);
            end loop;
        end loop;

        return samp_per_pulse_preamble;
    end function;

    constant expected_preamble : std_logic_vector(samp_per_preamble-1 downto 0) := get_preamble_array;

    signal threshold    : unsigned((2*sampWidth - 1 ) downto 0) := (others => '0');
    signal smp          : unsigned ((2*sampWidth - 1) downto 0) := (others => '0');

    signal smpCtr       : integer := packet_size + 1;
    signal value        : std_logic := '0';   --< is current sample interpreted as a '1' or '0'?
    signal valid        : std_logic := '0';   --< mode s preamble detected?
    signal smp_vld_flg	: std_logic := '0';
    signal validSmp		: std_logic := '0';
    signal somFlg		: std_logic := '0';
    signal eomFlg		: std_logic := '0';
    signal eofCtr		: integer := 0;

    signal processing_rest_of_packet : std_logic := '0';

    signal input_preamble : std_logic_vector(samp_per_preamble-1 downto 0) := (others => '0');

begin

    -- decide how to interpret the value of this sample
    -- based upon the current threshold magnitude
    process(clk)
    begin    
        if (rising_edge(clk)) then
            if (reset = '1') then
                threshold <= (others => '0');
            elsif (smp_in_vld = '1' and smp_rdy = '1') then
                threshold <= unsigned(avg_pwr) * to_unsigned(preambleThreshold, sampWidth);
            end if;
        end if;
    end process;

    smp       <= resize(unsigned(smp_in), 2*sampWidth);

    value <= '1' when (std_logic_vector(smp) > std_logic_vector(threshold)) else '0';

    shiftin : process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                input_preamble <= (others => '0');
            elsif smp_in_vld = '1' then
                input_preamble(samp_per_preamble-1 downto 0) <= input_preamble(samp_per_preamble-2 downto 0) & value;
            end if;
        end if;
    end process shiftin;

    valid <= '1' when (input_preamble = expected_preamble) and (processing_rest_of_packet = '0') else '0';

    -- Outputs samples and sample_valid once the preamble is detected
    -- Sample_valid is HIGH based on the 112-bit ADS-B message and
    -- and number of samples per symbol.  
    outputProc : process(clk)
    begin
        if (reset = '1') then
            smpCtr <= (packet_size - 1);
            smp_vld <= '0';
            validSmp <= '0';
        elsif (rising_edge(clk)) then
            if (smp_in_vld = '1' and smp_rdy = '1') then
                processing_rest_of_packet <= '0'; -- FIXME unsure where this needs to go
                if (valid = '1' or smpCtr < (packet_size - 1)) then
                    processing_rest_of_packet <= '1';
                    smp_out <= smp_in;
                    smp_vld <= '1';
                    validSmp <= '1';
                    if (smpCtr = 0 ) then
                        smpCtr <= (packet_size - 1);
                    else
                        smpCtr <= smpCtr - 1;
                    end if;
                else
                    smp_out <= smp_in;
                    smp_vld <= '0';
                    validSmp <= '0';   
                end if;
            elsif (smp_rdy = '0' and validSmp = '1') then
                smp_vld <= '1';
            elsif (smp_rdy = '1' and validSmp = '1') then
                smp_vld <= '0';
                validSmp <= '0';
            else
                smp_out <= smp_in;
                smp_vld <= '0';
                validSmp <= '0';
            end if;
        end if; 
    end process outputProc;

    metadata : process(clk)
    begin
        if (rising_edge(clk)) then
            if (reset = '1') then
                smp_som <= '0';
                somFlg <= '0';
                smp_eom <= '0';
                eomFlg <= '0';
                smp_eof <= '0';
                eofCtr <= 0;
            elsif (smp_in_vld = '1' and smp_rdy = '1') then
                if (valid = '1' and smpCtr = (packet_size - 1) and somFlg = '0') then
                    smp_som <= '1';
                    somFlg <= '1';
                    eomFlg <= '0';
                elsif (smpCtr = 0 and eomFlg = '0') then
                    smp_eom <= '1';
                    eomFlg <= '1';
                    somFlg <= '0';
                else
                    smp_som <= '0';
                    smp_eom <= '0';
                end if;
            else
                smp_som <= '0';
                smp_eom <= '0';	
                if (smp_in_eof = '1') then
                    if  (eofCtr < (sampWidth - 1 )) then
                        eofCtr <= eofCtr + 1;
                    else
                        smp_eof <= '1';
                    end if;
                else
                    smp_eof <= '0';
                end if;	
            end if;
        end if;
    end process metadata;

end Behavioral;