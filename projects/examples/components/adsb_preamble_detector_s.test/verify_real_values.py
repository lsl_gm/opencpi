#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import numpy

"""
Use this script to validate your output data against your input data.
Args: <output-file> <input-files>
"""
    # input_file1 = open(sys.argv[-1], 'rb')
    # input_file2 = open(sys.argv[-2], 'rb')

    # input_message_size1 = numpy.fromfile(input_file1, dtype=numpy.uint32, count=1)
    # input_opcode1 = numpy.fromfile(input_file1, dtype=numpy.uint32, count=1)
    # input_data1 = numpy.fromfile(input_file1, dtype=numpy.int16, count=-1)

    # input_message_size2 = numpy.fromfile(input_file2, dtype=numpy.uint32, count=1)
    # input_opcode2 = numpy.fromfile(input_file2, dtype=numpy.uint32, count=1)
    # input_data2 = numpy.fromfile(input_file2, dtype=numpy.int16, count=-1)

def test_0(output_data):
    gr_file = sys.path[0] + "/gr_data_out.bin"
    gr_data = numpy.fromfile(gr_file, dtype='h', count=-1)

    expected_raw = ()

    for i in range(len(gr_data)):
        expected_raw += ((float)(gr_data[i]*pow(2,-15)),)

    print("len of expected output: ")
    print(len(expected_raw))
    # print(expected_raw)
    # print(gr_data[16:240])
    print("")
    # print(gr_data[256:480])

    # expected_output = expected_raw

    print("len of actual output: ")
    print(len(output_data))
    # print(output_data[0:224])
    print("")
    # print(output_data[224:448])


def test_1(output_data):
    expected = []
    for i in range(33,len(pos_pac),2):
        expected.append(pos_pac[i])
    # print(expected)

    #print(len(output_data))
    # print(output_data[0:224])
    # print(output_data)

    for i in range(len(output_data)):
        if (output_data[i] != expected[i]):
            print("Miss match at: " + str(i))
            print(output_data)
            sys.exit(1)
        else:
            if (i == 223):
                #print("First packet good")
                break
            continue

pos_pac = [
    8, 19, 3, 0, 13, 24, 2, 3, 1, 1, 1, 2, 3, 5, 27, 31, 6, 1, 21, 28, 2, 5, 5, 3, 2, 4, 6, 1, 2, 4, 4, 2, # this line is preamble
    1, 3, 11, 26, 30, 25, 5, 4, 5, 5, 28, 47, 42, 34, 9, 2, 18, 40, 5, 2, 21, 47, 15, 10, 5, 4, 27, 31, 29, 24, 2, 0,
    12, 19, 4, 0, 2, 8, 21, 41, 42, 37, 7, 4, 3, 4, 19, 41, 40, 45, 17, 2, 10, 23, 3, 2, 4, 3, 18, 25, 3, 5, 23, 47,
    50, 37, 13, 1, 3, 2, 16, 37, 5, 4, 90, 106, 11, 0, 8, 40, 46, 22, 5, 5, 3, 22, 101, 58, 9, 7, 21, 77, 128, 65, 7, 3,
    15, 27, 6, 0, 0, 2, 27, 95, 42, 1, 20, 62, 48, 24, 0, 1, 1, 1, 24, 135, 172, 47, 7, 19, 146, 89, 6, 2, 22, 36, 19, 70,
    89, 33, 5, 9, 80, 179, 57, 6, 38, 139, 60, 5, 2, 2, 34, 120, 82, 29, 5, 2, 37, 143, 126, 105, 1147, 237, 36, 106, 41, 9, 1, 0,
    0, 35, 77, 373, 984, 56, 59, 107, 55, 1078, 655, 18, 51, 50, 167, 1246, 338, 13, 7, 5, 4, 2, 17, 44, 88, 16, 503, 1211, 73, 29, 136, 131,
    33, 2, 13, 29, 56, 79, 25, 21, 7, 69, 44, 18, 2, 1, 10, 16, 0, 366, 1206, 153, 0, 7, 475, 1152, 212, 23, 12, 18, 360, 443, 593, 8,
    2, 19, 308, 573, 496, 14, 3, 1, 259, 1473, 478, 17, 6, 1, 1, 3, 8, 10, 10, 7, 1, 2, 0, 5, 14, 508, 787, 53, 2, 5, 1, 0,
    2, 0, 1, 0, 2, 2, 2, 1, 1, 2, 3, 3, 2, 1, 5, 8, 3, 2, 3, 5, 5, 0, 0, 1, 3, 1, 2, 3, 2, 3, 2, 2,
    1, 2, 0, 0, 0, 1, 7, 249, 899, 171, 10, 12, 9, 4, 4, 0, 0, 3, 2, 0, 3, 3, 1, 1, 2, 0, 0, 0, 2, 5, 5, 3,
    1, 1, 3, 3, 2, 1, 1, 2, 0, 3, 2, 4, 3, 2, 2, 3, 4, 1, 2, 0, 0, 1, 1, 2, 0, 0, 0, 0, 2, 4, 1, 1,
    2, 0, 1, 2, 1, 0, 4, 5, 1, 3, 1, 0, 2, 2, 1, 4, 3, 1, 2, 1, 0, 0, 1, 1, 2, 0, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 3, 2, 0, 1, 0, 2, 4, 1, 0, 1, 3, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 4, 0, 0,
    1, 0, 1, 1, 1, 3, 5, 0, 0, 0, 1, 2, 0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 2, 4
]

if __name__ == '__main__':

    output_file = open(sys.argv[-3], 'rb')
    output_data = numpy.fromfile(output_file, dtype='<h', count=-1)

    test_number = int(sys.argv[1])

    if (test_number == 0):
        test_0(output_data)
    else:
        test_1(output_data)

    # for i in range(len(output_data)):
    #     if (output_data[i] == 321):
    #         print(i)
    #         break

    # if (len(output_data) != len(expected_raw)):
    #     exit(1)

    # result = ()

    # for i in range(len(output_data)):
    #     result += ((float)(output_data[i]*pow(2,-15)),)

    # print(result)

    # for i in range(0,len(result)):
    #     if (result[i] != expected_output[i]):
    #         print("Miss match at: " + str(i))
    #         sys.exit(1)
    #     else:
    #         if (i == 223):
    #             print("First packet good")
            # continue
    sys.exit(0)
